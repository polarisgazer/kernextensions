//
//  FileManager.swift
//  Chat
//
//  Created by Cristian Azov on 20.06.20.
//  Copyright © 2020 Cristian Azov. All rights reserved.
//

import Foundation

public extension FileManager {
    func fileUrl(
        for fileName: String,
        at path: String? = nil,
        in searchPathDirectory: FileManager.SearchPathDirectory = .cachesDirectory,
        with searchPathDomainMask: FileManager.SearchPathDomainMask = .userDomainMask
    ) throws -> URL {
        guard var rootDir = urls(for: searchPathDirectory, in: searchPathDomainMask).first else {
            throw FileManagerError.rootDirNotFound(searchPathDirectory, searchPathDomainMask)
        }
        guard !fileName.isEmpty else { throw FileManagerError.emptyFileName }
        let slash = "/"

        // Normailze and append path if needed.
        if let components = path?.components(separatedBy: slash).filter({ !$0.isEmpty }), !components.isEmpty {
            let filePath = components.joined(separator: slash)
            rootDir.appendPathComponent(filePath)
        }

        // Normalize file name, remove restricted characters.
        var name = fileName
        let invalidCharacters = [slash] // Add more here.
        for item in invalidCharacters {
            if name.contains(item) {
                name = fileName.replacingOccurrences(of: slash, with: "")
            }
        }

        return rootDir.appendingPathComponent(name)
    }
}

public enum FileManagerError: Error {
    case emptyFileName
    case rootDirNotFound(FileManager.SearchPathDirectory, FileManager.SearchPathDomainMask)
}
