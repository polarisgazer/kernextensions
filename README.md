# KernExtensions

[![CI Status](https://img.shields.io/travis/polarisgazer/KernExtensions.svg?style=flat)](https://travis-ci.org/polarisgazer/KernExtensions)
[![Version](https://img.shields.io/cocoapods/v/KernExtensions.svg?style=flat)](https://cocoapods.org/pods/KernExtensions)
[![License](https://img.shields.io/cocoapods/l/KernExtensions.svg?style=flat)](https://cocoapods.org/pods/KernExtensions)
[![Platform](https://img.shields.io/cocoapods/p/KernExtensions.svg?style=flat)](https://cocoapods.org/pods/KernExtensions)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KernExtensions is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following lines to your Podfile:

```ruby
source 'git@bitbucket.org:polarisgazer/kern-specs.git'

pod 'KernExtensions'
```

## Author

Cristian Azov, polarisgazer@gmail.com

## License

KernExtensions is available under the MIT license. See the LICENSE file for more info.
