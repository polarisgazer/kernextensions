//
//  FileManagerTests.swift
//  KernExtensions_Tests
//
//  Created by Cristian Azov on 28.08.22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import KernExtensions

class FileManagerTests: XCTestCase {
    private static let fileName = "file-name-1"

    func testSave() throws {
        let manager = FileManager.default
        let url = try manager.fileUrl(for: Self.fileName)
        XCTAssertFalse(manager.fileExists(atPath: url.path))

        let json = "{ \"name\"=\"John Smith\" }"
        let data = json.data(using: .utf8)!
        try data.write(to: url)

        XCTAssertTrue(manager.fileExists(atPath: url.path))

        let retrievedData = try Data(contentsOf: url)
        let dataStr = String(data: retrievedData, encoding: .utf8)
        XCTAssertEqual(dataStr, json)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        let manager = FileManager.default
        let url = try manager.fileUrl(for: Self.fileName)
        try FileManager.default.removeItem(at: url)
    }
}
