import XCTest
import KernExtensions

class Tests: XCTestCase {

  func testExample() throws {
    let savedData = try XCTUnwrap("Lorem".data(using: .utf8))
    try FileManager.default.save(data: savedData, fileName: "ipsum")
    let fetchedData = try FileManager.default.fetchData(fileName: "ipsum")
    XCTAssertEqual(savedData, fetchedData)
  }
}
