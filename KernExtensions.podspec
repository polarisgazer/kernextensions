#
# Be sure to run `pod lib lint KernExtensions.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KernExtensions'
  s.version          = '0.1.0'
  s.summary          = 'Core extensions for the KernProject'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  This library contains core extensions for the KernProject
                       DESC

  s.homepage         = 'https://bitbucket.org/polarisgazer/kernextensions/src'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Cristian Azov' => 'polarisgazer@gmail.com' }
  s.source           = { :git => 'https://polarisgazer@bitbucket.org/polarisgazer/kernextensions.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '13.0'

  s.source_files = 'KernExtensions/Classes/**/*'
  s.swift_versions = ['5']

  # s.resource_bundles = {
  #   'KernExtensions' => ['KernExtensions/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
